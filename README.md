<!--
## Hi, I'm Gringiemar Felix

I'm a Full-Stack Developer and I like coffee.
-->

![Header](https://gitlab.com/gringiemar.ideahub/gringiemar.ideahub/-/raw/main/header.png)

---

<!-- ### My Skills -->

![SkillsBadge](https://gitlab.com/gringiemar.ideahub/gringiemar.ideahub/-/raw/main/my-skills.svg)

### Front End

![FrontEnd](https://skillicons.dev/icons?i=html,css,sass,js,ts,react,nextjs,bootstrap,tailwind,jquery,vite,webpack,materialui)

### Back End

![BackEnd](https://skillicons.dev/icons?i=php,mysql,laravel,linux)

### Mobile

![Mobile](https://skillicons.dev/icons?i=react,androidstudio)

### Cloud

![Cloud](https://skillicons.dev/icons?i=gcp)

### Tools

![Tools](https://skillicons.dev/icons?i=git,github,gitlab,vscode,md,powershell,bash)

<!-- ### I speak: -->
![I Speak](https://gitlab.com/gringiemar.ideahub/gringiemar.ideahub/-/raw/main/i-speak.svg)
- 🇵🇭 Filipino
- 🇺🇸 English

<!-- ### Links: -->
![I Speak](https://gitlab.com/gringiemar.ideahub/gringiemar.ideahub/-/raw/main/my-links.svg)
- [Portfolio](https://gringiemarfelix.com/)
- [Curriculum Vitae](https://gringiemarfelix.com/cv)
- [LinkedIn](https://www.linkedin.com/in/gringiemarfelix)

<!-- Header by https://leviarista.github.io/github-profile-header-generator/ -- >
<!-- Icons by https://github.com/tandpfun/skill-icons -->
<!-- For the badge https://forthebadge.com/generator/ -- >
